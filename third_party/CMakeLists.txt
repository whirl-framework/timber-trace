include(FetchContent)

# --------------------------------------------------------------------

# Offline mode
# set(FETCHCONTENT_FULLY_DISCONNECTED ON)
# set(FETCHCONTENT_QUIET OFF)

# --------------------------------------------------------------------

# Runtime

FetchContent_Declare(
        compass
        GIT_REPOSITORY https://gitlab.com/Lipovsky/compass.git
        GIT_TAG master
)
FetchContent_MakeAvailable(compass)

# --------------------------------------------------------------------

# Context

FetchContent_Declare(
        carry
        GIT_REPOSITORY https://gitlab.com/Lipovsky/carry.git
        GIT_TAG master
)
FetchContent_MakeAvailable(carry)

# --------------------------------------------------------------------

# Logging

FetchContent_Declare(
        timber
        GIT_REPOSITORY https://gitlab.com/whirl-framework/timber.git
        GIT_TAG master
)
FetchContent_MakeAvailable(timber)
