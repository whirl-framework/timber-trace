#include <timber/trace/tracer.hpp>

#include <timber/trace/scope.hpp>
#include <timber/trace/log.hpp>

#include <carry/carrier.hpp>

#include <compass/map.hpp>

#include <fmt/core.h>


using namespace timber;

//////////////////////////////////////////////////////////////////////

class IdGenerator {
 public:
  trace::Id Generate() {
    return fmt::format("ID-{}", ++next_);
  }

 private:
  uint64_t next_ = 0;
};

//////////////////////////////////////////////////////////////////////

class Tracer :
    public trace::ITracer,
    public carry::ICarrier,
    public compass::Locator<trace::ITracer>,
    public compass::Locator<carry::ICarrier> {

 public:
  friend class Span;

  class Span : public trace::impl::Span {
   public:
    Span(Tracer& tracer, std::string name, trace::References refs)
      : tracer_(&tracer),
        trace_id_(tracer.GetTraceId(refs)),
        id_(tracer.GenerateId()),
        name_(name),
        refs_(refs) {
      //
    }

    // Actions

    void Finish() override {
      tracer_->Finish(*this);
    }

    void Cancel() override {
      tracer_->Cancel(*this);
    }

    // Getters

    const std::string& Name() const override {
      return name_;
    }

    trace::Id TraceId() const override {
      return trace_id_;
    }

    trace::Id SpanId() const override {
      return id_;
    }

    const trace::References& References() const override {
      return refs_;
    }

    trace::SpanContext Context() const override {
      return trace::SpanContext(trace_id_, id_);
    }

   private:
    Tracer* tracer_;

    trace::Id trace_id_;
    trace::Id id_;
    std::string name_;

    trace::References refs_;
  };

 public:
  // ITracer

  trace::Span StartSpan(std::string name, trace::References refs) override {
    // Assume single parent
    assert(refs.IsChild() || refs.IsEmpty());

    auto impl = std::make_shared<Span>(*this, name, refs);
    return {impl};
  }

  // carry::ICarrier

  void Set(carry::Context context) override {
    carry_ = std::move(context);
  }

  carry::Context& GetContext() override {
    return carry_;
  }

  // Runtime locators

  trace::ITracer* Locate(compass::Tag<trace::ITracer>) override {
    return this;
  }

  carry::ICarrier* Locate(compass::Tag<carry::ICarrier>) override {
    return this;
  }

  // Tracer

  std::vector<Span> Spans() {
    return spans_;
  }

 private:
  void Finish(Span& span) {
    spans_.push_back(span);
  }

  void Cancel(Span& /*ignore*/) {
    // Ignore
  }

  trace::Id GenerateId() {
    return ids_.Generate();
  }

  trace::Id GetTraceId(const trace::References& refs) {
    if (refs.IsEmpty()) {
      return GenerateId();
    } else {
      return refs.Parent().TraceId();
    }
  }

 private:
  std::vector<Span> spans_;

  // Context carrier
  carry::Context carry_;

  IdGenerator ids_;
};

//////////////////////////////////////////////////////////////////////

int main() {
  Tracer tracer;

  compass::Map().Add<trace::ITracer>(tracer);
  compass::Map().Add<carry::ICarrier>(tracer);

  {
    trace::ScopedSpan root("root");

    {
      trace::ScopedSpan x("x");

      {
        trace::ScopedSpan xx("xx");
      }

      {
        trace::ScopedSpan xy("xy");
      }
    }

    {
      trace::ScopedSpan y("y");
    }
  }

  {
    trace::ScopedSpan a("a");

    {
      trace::ScopedSpan b("b");
    }
  }

  auto spans = tracer.Spans();

  for (const auto& span : spans) {
    std::cout << span.Name()
              << " [" << span.TraceId() << " / " << span.SpanId() << "]"
              << ", refs = " << span.References()
              << std::endl;
  }

  return 0;
}
