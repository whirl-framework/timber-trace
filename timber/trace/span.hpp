#pragma once

#include <timber/trace/context.hpp>
#include <timber/trace/references.hpp>

#include <memory>

namespace timber::trace {

//////////////////////////////////////////////////////////////////////

namespace impl {

struct Span {
  virtual ~Span() = default;

  virtual const std::string &Name() const = 0;

  virtual SpanContext Context() const = 0;

  virtual Id TraceId() const = 0;
  virtual Id SpanId() const = 0;

  virtual const References& References() const = 0;

  // One-shot
  virtual void Finish() = 0;
  virtual void Cancel() = 0;
};

}  // namespace impl

//////////////////////////////////////////////////////////////////////

class Span {
  using ImplRef = std::shared_ptr<impl::Span>;
 public:
  Span(ImplRef impl)
    : impl_(std::move(impl)) {
  }

  ~Span() {
    if (impl_) {
      impl_->Cancel();
    }
  }

  const std::string& Name() const {
    return impl_->Name();
  }

  SpanContext Context() const {
    return impl_->Context();
  }

  Id TraceId() {
    return impl_->TraceId();
  }

  Id SpanId() {
    return impl_->SpanId();
  }

  const References& References() const {
    return impl_->References();
  }

  void Finish()&& {
    ReleaseImpl()->Finish();
  }

  void Cancel()&& {
    ReleaseImpl()->Cancel();
  }

 private:
  ImplRef ReleaseImpl() {
    return std::move(impl_);
  }

 private:
  ImplRef impl_;
};

}  // namespace timber::trace
