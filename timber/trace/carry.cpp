#include <timber/trace/carry.hpp>

namespace timber::trace {

std::string ContextKey() {
  return "timber.span";
}

carry::Context Wrap(carry::Context context, Span span) {
  return carry::Wrap(std::move(context))
      .Set(ContextKey(), std::move(span))
      .Done();
}

}  // namespace timber::trace
