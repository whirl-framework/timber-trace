#pragma once

#include <timber/trace/tracer.hpp>

#include <carry/scope.hpp>

namespace timber::trace {

class ScopedSpan {
 public:
  // Start span
  ScopedSpan(std::string name, ITracer& tracer);
  ScopedSpan(std::string name);

  // Finish span
  ~ScopedSpan();

 private:
  Span span_;
  carry::WrapScope<Span> scope_;
};

}  // namespace timber::trace
