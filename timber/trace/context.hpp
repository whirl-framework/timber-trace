#pragma once

#include <timber/trace/id.hpp>

namespace timber::trace {

class SpanContext {
 public:
  SpanContext(Id trace, Id span)
      : trace_(trace),
        span_(span) {
  }

  Id TraceId() const {
    return trace_;
  }

  Id SpanId() const {
    return span_;
  }

  bool Traced() const {
    return true;
  }

 private:
  Id trace_;
  Id span_;
};

}  // namespace timber::trace
