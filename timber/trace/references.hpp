#pragma once

#include <timber/trace/context.hpp>

#include <vector>

namespace timber::trace {

struct References {
  References() = default;

  References& AddParent(SpanContext context) {
    parents.push_back(context);
    return *this;
  }

  static References ChildOf(SpanContext parent) {
    return References().AddParent(parent);
  }

  static References Empty() {
    return {};
  }

  bool IsEmpty() const {
    return parents.empty();
  }

  bool IsChild() const {
    return parents.size() == 1;
  }

  // Precondition: IsChild()
  SpanContext Parent() const {
    return parents.front();
  }

  bool IsBatch() const {
    return parents.size() > 1;
  }

  std::vector<SpanContext> parents;
};

}  // namespace timber::trace
