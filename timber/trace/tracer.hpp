#pragma once

#include <timber/trace/span.hpp>

namespace timber::trace {

struct ITracer {
  virtual ~ITracer() = default;

  // + Inject / Extract

  virtual Span StartSpan(std::string name, References refs) = 0;
};

}  // namespace timber::trace
