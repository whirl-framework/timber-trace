#pragma once

#include <timber/trace/references.hpp>

#include <ostream>

namespace timber::trace {

// Logging support

std::ostream& operator<<(std::ostream& out, const References& refs);

}  // namespace timber::trace
