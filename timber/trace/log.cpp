#include <timber/trace/log.hpp>

namespace timber::trace {

std::ostream& operator<<(std::ostream& out, const References &refs) {
  if (refs.IsEmpty()) {
    out << "{}";
    return out;
  }

  if (refs.IsChild()) {
    out << refs.Parent().SpanId();
    return out;
  }

  out << '{';

  for (size_t i = 0; i < refs.parents.size(); ++i) {
    out << refs.parents[i].SpanId();
    if (i + 1 < refs.parents.size()) {
      out << ", ";
    }
  }

  out << '}';

  return out;
}

}  // namespace timber::trace
