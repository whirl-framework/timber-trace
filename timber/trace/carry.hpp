#pragma once

#include <timber/trace/span.hpp>

#include <carry/wrap.hpp>

#include <string>

namespace timber::trace {

std::string ContextKey();

carry::Context Wrap(carry::Context context, Span span);

}  // namespace timber::trace
