#pragma once

#include <string>

namespace timber::trace {

using Id = std::string;

}  // namespace timber::trace
