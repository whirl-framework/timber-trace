#include <timber/trace/scope.hpp>

#include <carry/current.hpp>

#include <timber/trace/carry.hpp>

#include <compass/locate.hpp>

namespace timber::trace {

static References CurrentSpanRefs() {
  auto curr_context = carry::Current();

  if (auto span = curr_context.TryGet<Span>(ContextKey())) {
    return References::ChildOf(span->Context());
  } else {
    return References::Empty();
  }
}

ScopedSpan::ScopedSpan(std::string name, ITracer& tracer)
  : span_(tracer.StartSpan(name, CurrentSpanRefs())),
    scope_(ContextKey(), span_) {
}

ScopedSpan::ScopedSpan(std::string name)
  : ScopedSpan(std::move(name), ::compass::Locate<ITracer>()) {
}

ScopedSpan::~ScopedSpan() {
  std::move(span_).Finish();
}

}  // namespace timber::trace
